import requests

__base_url = 'https://vip.bitcoin.co.id/api/{}/{}'


def ticker(pair: str = 'btc_idr'):
    return requests.get(__base_url.format(pair, 'ticker')).json()


def trades(pair: str = 'btc_idr'):
    return requests.get(__base_url.format(pair, 'trades')).json()


def depth(pair: str = 'btc_idr'):
    return requests.get(__base_url.format(pair, 'depth')).json()


def history(start_time: int, end_time: int, resolution: str = 'D', pair: str = 'btc_idr'):

    if resolution not in ['1', '5', '15', '60', 'D']:
        raise KeyError

    pair = ''.join([i.upper() for i in pair.strip().split('_')])
    historical_url = 'https://api2.bitcoin.co.id/tradingview/history?symbol={}&resolution={}&from={}&to={}'\
        .format(pair, resolution, start_time, end_time)

    return requests.get(historical_url).json()
