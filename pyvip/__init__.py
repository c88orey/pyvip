import requests
import urllib
import hmac
import hashlib
import time


__version__ = '0.1.0'
__author__ = 'Agung Pratama'
__all__ = ['Client', 'public']


class Client(object):
    __base_url = 'https://vip.bitcoin.co.id/tapi/'
    trade_history_key = ['count', 'from_id', 'end_id', 'order', 'since', 'end', 'pair']
    trx_type_key = ['buy', 'sell']
    order_choice = ['asc', 'desc']

    def __init__(self, key: str, secret: str):
        self.__key = key.encode()
        self.__secret = secret.encode()
        self.__session = requests.session()
        self.__session.trust_env = False

    def trust_env(self, trust: bool=False):
        self.__session.trust_env = trust

    def __call__(self, params):
        params['nonce'] = int(time.time() * 10000000)
        sign = hmac.new(self.__secret, urllib.parse.urlencode(params).encode(), hashlib.sha512).hexdigest()
        headers = {
            'Key': self.__key,
            'Sign': sign
        }
        return self.__session.post(self.__base_url, headers=headers, data=params).json()

    def get_info(self):
        return self.__call__({
            'method': 'getInfo'
        })

    def trans_history(self):
        return self.__call__({
            'method': 'transHistory'
        })

    def trade_history(self, pair: str = 'btc_idr', **kwargs):
        params = {
            'method': 'tradeHistory',
            'pair': pair,
        }

        for key, value in kwargs.items():
            if key in self.trade_history_key:
                if key == 'order' and value not in self.order_choice:
                    raise KeyError('Unknown \'{}\' order type'.format(value))
                params[key] = value
            else:
                raise KeyError('Unknown key \'{}\''.format(key))

        return self.__call__(params)

    def open_orders(self, pair: str = None):
        params = {
            'method': 'openOrders',
        }

        if pair:
            params['pair'] = pair

        return self.__call__(params)

    def order_history(self, pair: str = 'btc_idr', order_count: int = 100, order_from: int = 0):
        return self.__call__({
            'method': 'orderHistory',
            'pair': pair,
            'order': order_count,
            'from': order_from,
        })

    def get_order(self, order_id: int, pair: str = 'btc_idr'):
        return self.__call__({
            'method': 'getOrder',
            'order_id': order_id,
            'pair': pair,
        })

    def cancel_order(self, order_id: int, trx_type: str, pair: str = 'btc_idr'):
        return self.__call__({
            'method': 'cancelOrder',
            'pair': pair,
            'order_id': order_id,
            'type': trx_type
        })

    def trade(self, pair: str, trx_type: str, price: float, amount: float):
        params = {
            'method': 'trade',
            'pair': pair,
            'type': trx_type,
            'price': price
        }

        symbol = pair.split('_')[0]

        if trx_type not in self.trx_type_key:
            raise ValueError('Unknown transaction type {}'.format(trx_type))

        if pair == 'btc_idr':
            if trx_type == 'buy':
                params['idr'] = amount
            else:
                params['btc'] = amount
        else:
            params[symbol] = amount

        return self.__call__(params)

    def __del__(self):
        self.__session.close()
